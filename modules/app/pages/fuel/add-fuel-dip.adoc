= Add Fuel Dip

[TIP]
xref:portal:./management/fuel.adoc#assets[Setup your fuel assets] and xref:../asset/tag-assignment.adoc[link the tags] performing a dip

[#steps]
== Steps
. xref:employee-login.adoc#steps[Login] to the application as a fuel supervisor
. Click on the *Dip* button in the bottom navigation menu
. Scan the asset that is being dipped
. Enter the total litres that have been measured
. Click on the green button with the arrow
. Scan the tag of the employee that took the measurement
. View new fuel dip item in the summary list
.. If there is an issue with the item long press on the item to flag it for review
. View or edit the item on the portal under xref:portal:./data-review/overview.adoc[data review]

// TODO: Add video
// video::n0I6WsUVckE[youtube, width=350, height=500]

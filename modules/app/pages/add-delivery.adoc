= Add Delivery Item

[#steps]
== Steps
. Click on the delivery button in the bottom navigation menu
. Select the variant that is being delivered
. Select the block from which the variant is from
.. These blocks are filtered according to the variant that is planted on them
. Enter the number of units being delivered by the driver
.. The transfer unit is displayed here. Progress to the next screen by pressing enter or on the arrow.
.. This can be configured on the portal under xref:portal:./management/variants.adoc[variant management]  
.. The variant can also be setup with multiple transfer unit conversions on the portal under xref:portal:./management/variants.adoc[variant management]  
... If the variant has multiple conversions, first select the correct option and then proceed with entering the number of units 
. Scan the driver tag to assign the entered number of delivery items to the employee
.. If tag is scanned successfully a confirmation message will be displayed.
.. Optionally click on lost tag button to enter an employee number and assign the harvested units to the corresponding employee
. View new delivery item in the summary list
.. If there is an issue with the item long press on the item to flag it for review
. View or edit the item on the portal under xref:portal:./data-review/overview.adoc[data review]

video::uTqIkNLQ0Zg[youtube, width=350, height=500]

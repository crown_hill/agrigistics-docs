= User Access
Users with the following roles login to the app using their username and password:

* Administrators
* Farmers
* Scouts
* Chemical Technicians 

Employees with the following roles login to the app using their assigned tag:

* Foreman
* Receiver
* Sale
* Pack Supervisor
* Fuel
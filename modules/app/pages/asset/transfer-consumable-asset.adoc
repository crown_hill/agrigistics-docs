= Transfer Consumable Asset

[NOTE]
Ensure that your xref:portal:./management/assets.adoc[assets] have been configured and set as consumable assets

[#steps]
== Steps
. xref:employee-login.adoc#steps[Login] to the application as a foreman
. Click on the three dots in the top right corner
. Click on *Asset Transfer*
. Click on *Transfer*
. Choose whether the asset is being issued or received by selecting either *Issue* or *Receive*
. Select the category of the asset
. Select the group of the asset
. Select the asset
. Enter the number of items being issued or received
. Press the green arrow button
. Scan the employee's tag responsible for receiving or returning the asset
    .. The tags can be scanned one after the other without any additional input
. View the new asset transfer item in the summary list by returning to the overview screen
.. If there is an issue with the item long press on the item to flag it for review
. View or edit the item on the portal under xref:portal:./data-review/overview.adoc[data review]

//TODO: Update video 
video::INYm-sWFt-c[youtube, width=350, height=500]
 
= Employee Overview

== Navigation

In the navigation menu click on: Reports > Manager > Employee Overview

== Overview

This report combines all of the clock and activity items per employee into a single report and provides a summarized view of an employees hours and activities per day. 

== Report Filters

image:https://agrigistics.co.za/wp-content/uploads/2021/09/emp_overview_legend.jpg[Employee overview report filters, width=60%, align=center]

.Report filters
[%header,cols=3,%autowidth.stretch] 
|===
|Index
|Column
|Description

|1
|Date selector
|Select the date range for the report

|2
|Employee Team filter
|Select the team filter for the report

|3
|Employee filter 
|Select the employee filter for the report

|4
|Override template defaults
|Check this box to override default config in the clock templates. To configure the clock templates with defaults visit xref:./management/employees.adoc#clock-template-add[this, window=_blank] page.
 
|5
|Overtime flex (Disabled if 4 is selected)
|If an employee is clocked in or out within this time period outside of their normal working hours it will not contribute to their working hours. For example with overtime flex of 30 minutes, if the end of shift is 17:00 and the employee clocks out at 17:15 then the extra 15 minutes will not contribute.

|6
|Default to shift end if clock out missing (Disabled if 4 is selected)
|Set clock out time to end of shift if there is no clock out

|7
|Piecework overlap
|Indicate employees where there is an overlap between piecework and clocked-in activities

|8
|Generate
|Generate the report

|9
|Search employee
|Search for a specific employee

|10
|Detailed view
|Enable the detailed view of the report

|11
|Export
|Export the data to an Excel sheet

|===

== Result

image:https://agrigistics.co.za/wp-content/uploads/2021/08/employee_overview_hours.png[Report result, align=center]

.Table columns
[%header,cols=3*] 
|===
|Index
|Column
|Description

|1
|Emp. No.
|Employee number

|2
|Employee
|Employee full name

|3
|Total Hours
a|Employee hours broken into specific categories

[cols=2*] 
!===
!N
!Normal

!OT
!Overtime

!SAT
!Saturday

!SUN
!Sunday

!PH
!Public holiday

!===

|4
|Leave
|Display leave for employee

|5
|Dates
|Summary of the hours for the day split up into the various categories

|===

== Detailed view
image:https://agrigistics.co.za/wp-content/uploads/2021/08/employee_overview_detailed.jpeg[Detailed view, align=center]

A detailed overview of the data can be enabled to view the hours spent per activity and the measured activities for the day.

image:https://agrigistics.co.za/wp-content/uploads/2021/08/employee_overview_detailed_day.jpeg[Detailed view - day, align=center]

Here is a summary of a specific day with the hours split into the various activities and the total measured activities. It also indicates the total hours and overtime hours.

[#rep-employee-overview-add-missing]
== Add missing data

image:https://agrigistics.co.za/wp-content/uploads/2021/08/employee_overview_clock_out.jpeg[Add clock out, align=center]

If a clock in has been added for an employee but there is no clock out, add the clock out by pressing on the button.

image:https://agrigistics.co.za/wp-content/uploads/2021/08/employee_overview_clock_in.jpeg[Add clock in, align=center]

If a clock out has been added for an employee but there is no clock in, add the clock in by pressing on the button.


// [#rep-employee-overview-add-additional]
// == Add additional data

// image:https://agrigistics.co.za/wp-content/uploads/2021/08/employee_overview_add_data.jpeg[Add clock in, align=center]

// If a clock in and out exists for a day, additional data can be added for the specified day by clicking on the three dots next to the day's summary. The following data can be addded:

// . In & Out clock
// . Clock In
// . Clock out